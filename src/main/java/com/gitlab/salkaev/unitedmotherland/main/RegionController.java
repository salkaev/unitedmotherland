package com.gitlab.salkaev.unitedmotherland.main;

import com.gitlab.salkaev.unitedmotherland.logika.Game;
import com.gitlab.salkaev.unitedmotherland.logika.Observer;
import com.gitlab.salkaev.unitedmotherland.logika.Region;
import com.gitlab.salkaev.unitedmotherland.logika.Subject;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.Set;

public class RegionController {

    private Set<Observer> observerSet;

    Region region;
    Game game;
    Stage primaryStage;
    @FXML public Text populationText;
    @FXML public Text welfareText;
    @FXML public Text oppositionText;
    @FXML public Text taxText;
    @FXML public Text moneyPlusText;

    public void initialize(Game game, Region region,Stage primaryStage){
        this.primaryStage = primaryStage;
        this.game = game;
        this.region = region;
        update();
    }

    public void update(){
        populationText.setText("" + region.getPopulation() + " (" + (region.popTurn()>0?"+"+region.popTurn():"-"+region.popTurn())+")");
        welfareText.setText("" + region.getWelfare() + " (" + (region.welTurn()>0?"+"+region.welTurn():"-"+region.welTurn())+")");
        oppositionText.setText("" + region.getOpposition() + " (" + (region.oppTurn()>0?"+"+region.oppTurn():"-"+region.oppTurn())+")");
        taxText.setText("" + region.getTax());
        moneyPlusText.setText("Money +" + region.taxes());


    }

    @FXML public void minusClick(){
        region.setTax(region.getTax() - 1);
        update();
    }
    @FXML public void plusClick(){
        region.setTax(region.getTax() + 1);
        update();
    }

    @FXML public void regionMarketClick(){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/MarketScene.fxml"));
            VBox root1 =  fxmlLoader.load();
            MarketController marketController = fxmlLoader.getController();
            marketController.initialize(game,region,this);

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


}
