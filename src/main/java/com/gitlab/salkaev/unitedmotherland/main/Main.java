package com.gitlab.salkaev.unitedmotherland.main;

import com.gitlab.salkaev.unitedmotherland.logika.Game;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Main extends Application
{


    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/MainScene.fxml"));

        VBox rootLayout =  loader.load();
        MainController controller = loader.getController();
        controller.initialize(new Game(),primaryStage);
        primaryStage.setScene(new Scene(rootLayout));
        primaryStage.setTitle("United Motherland");
        primaryStage.setResizable(false);

        primaryStage.show();
    }

    public static void main(String[] args )
    {
        launch(args);
    }
}
