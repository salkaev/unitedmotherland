package com.gitlab.salkaev.unitedmotherland.main;

import com.gitlab.salkaev.unitedmotherland.logika.Game;
import com.gitlab.salkaev.unitedmotherland.logika.Goods;
import com.gitlab.salkaev.unitedmotherland.logika.Region;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;

import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.HashSet;

public class MarketController {

    private Game game;
    private Region region;
    private Goods goods;
    private RegionController regionController;
    private MainController mainController;
    private boolean flagMarket; //true - region market. false elite market.


    @FXML ListView goodsList;
    @FXML Text textPriceText;
    @FXML Text textPriceValue;
    @FXML Text textType;
    @FXML Text textTypeValue;

    public void initialize(Game game, Region region, RegionController regionController){

        flagMarket = true;
        this.regionController = regionController;
        this.region = region;
        this.game = game;
        goodsList.getItems().addAll(game.getRegionMarket().getGoodsSet());

        textPriceText.setText("");
        textPriceValue.setText("");
        textType.setText("");
        textTypeValue.setText("");

    }
    public void initialize(Game game, MainController mainController){

        this.mainController = mainController;
        flagMarket = false;
        this.game = game;
        goodsList.getItems().addAll(game.getEliteMarket().getGoodsSet());

        textPriceText.setText("");
        textPriceValue.setText("");
        textType.setText("");
        textTypeValue.setText("");
    }

    @FXML public void goodsClick(){

        String choosen = goodsList.getSelectionModel().getSelectedItem().toString();
        if (flagMarket) {
            goods = game.getRegionMarket().getGoodsMap().get(choosen);
        }else{
            goods = game.getEliteMarket().getGoodsMap().get(choosen);
        }

        String type;

        if(goods.getType() == 1){ type = "Elite point:";}else if(goods.getType() == 2){ type = "Welfare:";}else{ type = "Opposition:";}


        textPriceText.setText("Price:");
        textPriceValue.setText("" + goods.getPrice() + "(" + game.getMoney() + ")");
        textType.setText(type);
        String temp = "";
        if(goods.getType() == 1){ temp = "" + game.getElitePoint(); }
        if(goods.getType() == 2){ temp = "" + region.getWelfare(); }
        if(goods.getType() == 3){ temp = "" + region.getOpposition(); }
        textTypeValue.setText(((goods.getType()>2)?"-":"+") + goods.getPoint() + "(" + temp + ")");




    }

    @FXML public void buyClick(){

        if (flagMarket) {
            if(!game.buyRegionMarket(region, goods)){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Ooops!");
                alert.setHeaderText(null);
                alert.setContentText("You don't have enough money!");

                alert.showAndWait();
            }
            regionController.update();
        } else {
            if(!game.buyEliteMarket(goods)){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Ooops!");
                alert.setHeaderText(null);
                alert.setContentText("You don't have enough money!");

                alert.showAndWait();
            }
            mainController.update();

        }
        update();
    }

    public void update(){

        textPriceValue.setText("" + goods.getPrice() + "(" + game.getMoney() + ")");
        String temp = "";
        if(goods.getType() == 1){ temp = "" + game.getElitePoint(); }
        if(goods.getType() == 2){ temp = "" + region.getWelfare(); }
        if(goods.getType() == 3){ temp = "" + region.getOpposition(); }
        textTypeValue.setText(((goods.getType()>2)?"-":"+") + goods.getPoint() + "(" + temp + ")");
    }
}
