package com.gitlab.salkaev.unitedmotherland.main;

import com.gitlab.salkaev.unitedmotherland.logika.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;

import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;


import static javafx.scene.paint.Color.*;



import com.google.gson.*;


/***
 * Main controller to MainScene
 */
public class MainController extends VBox implements Observer {

    Stage primaryStage;

    HashMap<Rectangle, Region> regionMapFX = new HashMap<>();
    Game myGame;

    @FXML
    public Text turnText;
    @FXML
    public Text moneyText;
    @FXML
    public Text eliteClubPointText;
    @FXML
    public Button nextTurnButton;
    @FXML
    public Rectangle r1;
    @FXML
    public Rectangle r2;
    @FXML
    public Rectangle r3;
    @FXML
    public Rectangle r4;
    @FXML
    public Rectangle r5;
    @FXML
    public Rectangle r6;
    @FXML
    public Rectangle r7;
    @FXML
    public Rectangle r8;
    @FXML
    public Rectangle r9;
    @FXML
    public Rectangle r10;
    @FXML
    public Text untillElectionTurnText;
    @FXML
    public Text populationR1;
    @FXML
    public Text populationR2;
    @FXML
    public Text populationR3;
    @FXML
    public Text populationR4;
    @FXML
    public Text populationR5;
    @FXML
    public Text populationR6;
    @FXML
    public Text populationR7;
    @FXML
    public Text populationR8;
    @FXML
    public Text populationR9;
    @FXML
    public Text populationR10;

    /***
     * initialize game
     * @param game
     * @param primaryStage
     */
    public void initialize(Game game, Stage primaryStage) {
        myGame = game;
        this.primaryStage = primaryStage;
        myGame.addObserver(this);


        //assign rectanlge to region
        regionMapFX.put(r1, game.getRegionMap().get("r1"));
        regionMapFX.put(r2, game.getRegionMap().get("r2"));
        regionMapFX.put(r3, game.getRegionMap().get("r3"));
        regionMapFX.put(r4, game.getRegionMap().get("r4"));
        regionMapFX.put(r5, game.getRegionMap().get("r5"));
        regionMapFX.put(r6, game.getRegionMap().get("r6"));
        regionMapFX.put(r7, game.getRegionMap().get("r7"));
        regionMapFX.put(r8, game.getRegionMap().get("r8"));
        regionMapFX.put(r9, game.getRegionMap().get("r9"));
        regionMapFX.put(r10, game.getRegionMap().get("r10"));
        regionMapFX.forEach((k, v) -> {
            v.addObserver(this);
        });

        populationR1.setDisable(true);
        populationR2.setDisable(true);
        populationR3.setDisable(true);
        populationR4.setDisable(true);
        populationR5.setDisable(true);
        populationR6.setDisable(true);
        populationR7.setDisable(true);
        populationR8.setDisable(true);
        populationR9.setDisable(true);
        populationR10.setDisable(true);


        update();

    }

    /***
     * update all stage info
     */
    public void update() {
        turnText.setText("" + myGame.getTurn());
        moneyText.setText("" + myGame.getMoney());
        eliteClubPointText.setText("" + myGame.getElitePoint() + "/" + (myGame.getTurn() / 8 + 1) * 1000);
        untillElectionTurnText.setText("" + (8 - (myGame.getTurn() % 8)));


        regionTextInfoSet();
    }

    /***
     * set region info on rectangle
     */
    public void regionTextInfoSet() {

        int x = 10;
        int y = 15;
        System.out.println(r1.getLayoutX() + x + " " + r1.getLayoutY() + y);
        populationR1.setX(r1.getLayoutX() + x);
        populationR1.setY(r1.getLayoutY() + y);
        populationR1.setText("P:" + regionMapFX.get(r1).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r1).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r1).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r1).getTax()

        );


        populationR2.setX(r2.getLayoutX() + x);
        populationR2.setY(r2.getLayoutY() + y);
        populationR2.setText("P:" + regionMapFX.get(r2).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r2).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r2).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r2).getTax()
        );

        populationR3.setX(r3.getLayoutX() + x);
        populationR3.setY(r3.getLayoutY() + y);
        populationR3.setText("P:" + regionMapFX.get(r3).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r3).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r3).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r3).getTax()
        );

        populationR4.setX(r4.getLayoutX() + x);
        populationR4.setY(r4.getLayoutY() + y);
        populationR4.setText("P:" + regionMapFX.get(r4).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r4).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r4).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r4).getTax()
        );

        populationR5.setX(r5.getLayoutX() + x);
        populationR5.setY(r5.getLayoutY() + y);
        populationR5.setText("P:" + regionMapFX.get(r5).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r5).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r5).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r5).getTax()
        );

        populationR6.setX(r6.getLayoutX() + x);
        populationR6.setY(r6.getLayoutY() + y - 4);
        populationR6.setText("P:" + regionMapFX.get(r6).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r6).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r6).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r6).getTax()
        );

        populationR7.setX(r7.getLayoutX() + x);
        populationR7.setY(r7.getLayoutY() + y);
        populationR7.setText("P:" + regionMapFX.get(r7).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r7).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r7).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r7).getTax()
        );

        populationR8.setX(r8.getLayoutX() + x);
        populationR8.setY(r8.getLayoutY() + y);
        populationR8.setText("P:" + regionMapFX.get(r8).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r8).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r8).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r8).getTax()
        );

        populationR9.setX(r9.getLayoutX() + x);
        populationR9.setY(r9.getLayoutY() + y);
        populationR9.setText("P:" + regionMapFX.get(r9).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r9).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r9).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r9).getTax()
        );

        populationR10.setX(r10.getLayoutX() + x - 7);
        populationR10.setY(r10.getLayoutY() + y);
        populationR10.setText("P:" + regionMapFX.get(r10).getPopulation() + "\n" +
                "O:" + regionMapFX.get(r10).getOpposition() + (regionMapFX.get(r1).getOpposition() >= 90 ? "(R)" : "") + "\n" +
                "W:" + regionMapFX.get(r10).getWelfare() + "\n" +
                "T:" + regionMapFX.get(r10).getTax()
        );


        regionMapFX.forEach((k, v) -> {
            if (v.getOpposition() >= 51) {
                k.setFill(RED);
            } else if (v.getOpposition() >= 40) {
                k.setFill(YELLOWGREEN);
            } else {
                k.setFill(GREEN);
            }
        });

    }

    @FXML
    public void nextTurnButtonAction() {

        int election = myGame.election();
        if ((myGame.getTurn() % 8) == 7 && myGame.getTurn() != 0) {
            if (election >= 5 && myGame.getEliteCap() <= myGame.getElitePoint()) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Congratulation!!!");
                alert.setHeaderText(null);
                alert.setContentText("You win the election and still stay in the Elite club! " + election + " regions give vote for you!\n Try to save your power!!!");

                alert.showAndWait();
            } else if (myGame.getEliteCap() > myGame.getElitePoint()) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Oh no!!!");
                alert.setHeaderText(null);
                alert.setContentText("You lost your membership in Elite Club. You don't see the reason to life\nYou lasted only " + myGame.getTurn() + " turn!\nTHANKS FOR PLAYING!!!");
                alert.showAndWait();
                primaryStage.close();
            } else if (election < 5) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Country has left!!!");
                alert.setHeaderText(null);
                alert.setContentText("You win the election\n" + (10 - election) + " regions have voted against you!\nYou lasted only " + myGame.getTurn() + " turn!\nTHANKS FOR PLAYING!!!");
                alert.showAndWait();
                primaryStage.close();
            } else if (!(election > 5 && myGame.getEliteCap() < myGame.getElitePoint())) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Finally!!!");
                alert.setHeaderText(null);
                alert.setContentText("You have lost everything! Your country and the more important thing - your membership in Elite Club\n" + (10 - election) + " regions have voted against you!\nYou lasted only " + myGame.getTurn() + " turn!\nTHANKS FOR PLAYING!!!");

                alert.showAndWait();

                primaryStage.close();
            }

        }

        myGame.nextTurn();

        update();

    }

    @FXML
    public void eliteMarketButtonAction() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/MarketScene.fxml"));
            VBox root1 = fxmlLoader.load();
            MarketController marketController = fxmlLoader.getController();
            marketController.initialize(myGame, this);

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void clickR1() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r1), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @FXML
    private void clickR2() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r2), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void clickR3() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r3), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void clickR4() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r4), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void clickR5() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r5), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void clickR6() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r6), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void clickR7() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r7), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void clickR8() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r8), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void clickR9() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r9), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void clickR10() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RegionScene.fxml"));
            VBox root1 = fxmlLoader.load();
            RegionController regionController = fxmlLoader.getController();
            regionController.initialize(myGame, regionMapFX.get(r10), (Stage) r1.getScene().getWindow());

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * start new game
     */
    @FXML
    public void newGameMenuButton() {

        initialize(new Game(), primaryStage);
    }

    /***
     * close the game
     */
    @FXML
    public void quitGameMenuButton() {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Game closing");
        alert.setContentText("Do you actually want to close this awesome game?");
        ButtonType okButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType cancelButton = new ButtonType("Of course not", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(okButton, cancelButton);
        alert.showAndWait().ifPresent(type -> {
            if (type == okButton) {
                primaryStage.close();
            }
        });

    }

    /***
     * Help
     * @throws IOException
     */
    @FXML public void helpMenuButton() throws IOException {

        System.out.println(getClass().getResource("/HelpScene.fxml"));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/HelpScene.fxml"));
        VBox parent = loader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        stage.setScene(new Scene(parent));
        stage.show();
    }


    @FXML
    public void saveMenuButton() {

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Region.class, new RegionConverter());
        builder.registerTypeAdapter(Game.class, new GameConverter());
        Gson gson = builder.setPrettyPrinting().create();


        try (FileWriter file = new FileWriter("save.json")) {

            file.write(gson.toJson(myGame));
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    @FXML
    public void loadMenuButton() {

        Gson gson = new GsonBuilder().
                registerTypeAdapter(Game.class, new GameConverter()).
                registerTypeAdapter(Region.class, new RegionConverter()).
                create();
        Game savedGame = new Game();

        try (Reader reader = new FileReader("save.json")) {

            // Convert JSON to JsonElement, and later to String

            savedGame = gson.fromJson(reader, Game.class);


            System.out.println("pop: " + savedGame.getRegionMap().get("r1").getPopulation());
            System.out.println("opp: " + savedGame.getRegionMap().get("r1").getOpposition());
            System.out.println("wel: " + savedGame.getRegionMap().get("r1").getWelfare());
            System.out.println("tax: " + savedGame.getRegionMap().get("r1").getTax());

        } catch (IOException e) {
            e.printStackTrace();
        }

        myGame.setMoney(savedGame.getMoney());
        myGame.setElitePoint(savedGame.getElitePoint());
        myGame.setTurn(savedGame.getTurn());

        for (Map.Entry<String, Region> entry : savedGame.getRegionMap().entrySet()) {
            myGame.getRegionMap().get(entry.getKey()).setTax(entry.getValue().getTax());
            myGame.getRegionMap().get(entry.getKey()).setWelfare(entry.getValue().getWelfare());
            myGame.getRegionMap().get(entry.getKey()).setOpposition(entry.getValue().getOpposition());
            myGame.getRegionMap().get(entry.getKey()).setPopulation(entry.getValue().getPopulation());
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game loaded");
        alert.setHeaderText(null);
        alert.setContentText("Game loaded");

        alert.showAndWait();

    }
}
