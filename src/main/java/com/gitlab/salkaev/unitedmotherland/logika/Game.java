package com.gitlab.salkaev.unitedmotherland.logika;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class Game implements Subject  {

    private Set<Observer> observerSet;

    private int turn;
    private int money;
    private int elitePoint; // point in elite club
    private HashMap<String,Region> regionMap = new HashMap<>();
    private Market eliteMarket;
    private Market regionMarket;

    public Game(){

        observerSet = new HashSet<>();
        this.turn = 0;
        this.money = 1000;
        this.elitePoint = 0;

        //create region and neighbors
        regionMap.put("r1",new Region());
        regionMap.put("r2",new Region());
        regionMap.put("r3",new Region());
        regionMap.put("r4",new Region());
        regionMap.put("r5",new Region());
        regionMap.put("r6",new Region());
        regionMap.put("r7",new Region());
        regionMap.put("r8",new Region());
        regionMap.put("r9",new Region());
        regionMap.put("r10",new Region());

        regionMap.get("r1").addNeighbor(regionMap.get("r2"));
        regionMap.get("r1").addNeighbor(regionMap.get("r3"));


        regionMap.get("r2").addNeighbor(regionMap.get("r1"));
        regionMap.get("r2").addNeighbor(regionMap.get("r3"));
        regionMap.get("r2").addNeighbor(regionMap.get("r5"));



        regionMap.get("r3").addNeighbor(regionMap.get("r2"));
        regionMap.get("r3").addNeighbor(regionMap.get("r4"));
        regionMap.get("r3").addNeighbor(regionMap.get("r6"));


        regionMap.get("r4").addNeighbor(regionMap.get("r3"));
        regionMap.get("r4").addNeighbor(regionMap.get("r6"));
        regionMap.get("r4").addNeighbor(regionMap.get("r7"));


        regionMap.get("r5").addNeighbor(regionMap.get("r1"));
        regionMap.get("r5").addNeighbor(regionMap.get("r2"));
        regionMap.get("r5").addNeighbor(regionMap.get("r6"));


        regionMap.get("r6").addNeighbor(regionMap.get("r2"));
        regionMap.get("r6").addNeighbor(regionMap.get("r3"));
        regionMap.get("r6").addNeighbor(regionMap.get("r4"));
        regionMap.get("r6").addNeighbor(regionMap.get("r5"));
        regionMap.get("r6").addNeighbor(regionMap.get("r7"));
        regionMap.get("r6").addNeighbor(regionMap.get("r8"));
        regionMap.get("r6").addNeighbor(regionMap.get("r9"));



        regionMap.get("r7").addNeighbor(regionMap.get("r4"));
        regionMap.get("r7").addNeighbor(regionMap.get("r6"));
        regionMap.get("r7").addNeighbor(regionMap.get("r9"));
        regionMap.get("r7").addNeighbor(regionMap.get("r10"));


        regionMap.get("r8").addNeighbor(regionMap.get("r6"));
        regionMap.get("r8").addNeighbor(regionMap.get("r9"));


        regionMap.get("r9").addNeighbor(regionMap.get("r6"));
        regionMap.get("r9").addNeighbor(regionMap.get("r7"));
        regionMap.get("r9").addNeighbor(regionMap.get("r8"));
        regionMap.get("r9").addNeighbor(regionMap.get("r10"));


        regionMap.get("r10").addNeighbor(regionMap.get("r7"));
        regionMap.get("r10").addNeighbor(regionMap.get("r10"));

        //Elite market creation and filling
        eliteMarket = new Market();
        eliteMarket.addToMarket(new Goods("Yacht",1000, 750, 1));
        eliteMarket.addToMarket(new Goods("Car",500, 500,1));
        eliteMarket.addToMarket(new Goods("Private jet",2000, 1000,1));


        //Region market creation and filling
        regionMarket = new Market();
        regionMarket.addToMarket(new Goods("School (welfare)",1000, 12, 2));
        regionMarket.addToMarket(new Goods("Park (welfare)",500, 5, 2));
        regionMarket.addToMarket(new Goods("Hospital (welfare)",1500, 18, 2));
        regionMarket.addToMarket(new Goods("Roads (welfare)",2000, 25, 2));
        regionMarket.addToMarket(new Goods("public spaces (welfare)",3500, 45, 2));

        regionMarket.addToMarket(new Goods("Imprison (opposition)",500, 5, 3));
        regionMarket.addToMarket(new Goods("TV propaganda (opposition)",1000, 12, 3));
        regionMarket.addToMarket(new Goods("Assassination (opposition)",2000, 25, 3));
        regionMarket.addToMarket(new Goods("Terrorist attack (opposition)",3500, 45, 3));

    }

    /***
     * function which run all region turn-action.
     */
    public void regionTurn(){
        regionMap.forEach((k,v)->v.welfareTurn());
        regionMap.forEach((k,v)->v.oppositionTurn());
        regionMap.forEach((k,v)->v.popilationTurn());

    }

    /***
     * collect money from region
     */
    public void taxCollection(){
        regionMap.forEach((k,v)->plusMoney(v.taxes()));
    }

    public void nextTurn(){

        turn++;
        regionTurn();
        taxCollection();
        migrationTurn();


        notifyObserver();


    }


    public int election(){

         int temp = 0;
         for (Map.Entry<String, Region> entry : regionMap.entrySet()) {
                temp += entry.getValue().electionTurn();
         }
         return temp;

    }

    public boolean buyRegionMarket(Region region, Goods goods){
        if(money >= goods.getPrice()) {
            if(goods.getType()<3) {
                region.setWelfare(region.getWelfare() + goods.getPoint());
                setMoney(getMoney() - goods.getPrice());
                notifyObserver();
            }else {
                region.setOpposition(region.getOpposition() - goods.getPoint());
                setMoney(getMoney() - goods.getPrice());
                notifyObserver();
            }
            return true;
        }else{
            return false;
        }


    }

    public boolean buyEliteMarket(Goods goods){
        if(money >= goods.getPrice()) {
            setElitePoint(getElitePoint() + goods.getPoint());
            setMoney(getMoney() - goods.getPrice());
            notifyObserver();
            return true;
        }else{
            return false;
        }

    }

    public void migrationTurn(){


        for (Map.Entry<String, Region> entry : regionMap.entrySet()) {
            Region curRegion = entry.getValue();
            Region temp = curRegion;
            for (Region region: entry.getValue().getNeighbor()) {
                if (temp.getWelfare() < region.getWelfare()) {
                    temp = region;
                }
            }
            if (!curRegion.equals(temp)){
                int popolationMigrate = (int) (curRegion.getPopulation() * ((temp.getWelfare() - curRegion.getWelfare())/100));
                curRegion.setPopulation(curRegion.getPopulation() - popolationMigrate);
                temp.setPopulation(temp.getPopulation() + popolationMigrate);
            }
        }


    }




    /***
     * All get/set function
     */

    public int getTurn() {
        return turn;
    }

    public int getElitePoint() {
        return elitePoint;
    }

    public int getMoney() {
        return money;
    }

    public HashMap<String,Region> getRegionMap(){
        return regionMap;

    }

    public void setRegionMap(HashMap<String, Region> regionMap){

        this.regionMap = regionMap;
    }

    public void setMoney(int money) {
        this.money = money;
    }
    public void plusMoney(int money){
        this.money = this.money + money;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public void setElitePoint(int elitePoint) {
        this.elitePoint = elitePoint;
    }

    public Market getEliteMarket(){
        return eliteMarket;
    }

    public Market getRegionMarket() {
        return regionMarket;
    }

    @Override
    public void addObserver(Observer obj) {
        observerSet.add(obj);
    }

    @Override
    public void notifyObserver() {
        for (Observer observer: observerSet) {
            observer.update();
        }

    }

    public int getEliteCap(){
        return (8 - (turn % 8))*1000;
    }
}
