package com.gitlab.salkaev.unitedmotherland.logika;

public interface Subject {

    void addObserver(Observer obj);

    void notifyObserver();
}
