package com.gitlab.salkaev.unitedmotherland.logika;

public class Goods {

    private String name;
    private int price;
    private int point;
    private int type; // 1 - elite, 2 - region, 3 - special

    public Goods(String name, int price, int point, int type){
        this.type = type;
        this.name = name;
        this.point = point;
        this.price = price;

    }

    public String getName() {
        return name;
    }

    public int getPoint() {
        return point;
    }

    public int getPrice() {
        return price;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + price;
        result = 31 * result + point;
        return result;
    }
}

