package com.gitlab.salkaev.unitedmotherland.logika;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;


public class Region implements Subject{

    private Set<Observer> observerSet;

    private int population;
    private int opposition;
    private int welfare;
    private int tax;
    private HashSet<Region> neighbor;


    public Region (){

        observerSet = new HashSet<>();
        Random randomGenerator = new Random();
        this.population = 90 + randomGenerator.nextInt(20) + 1;
        this.opposition = 15 + randomGenerator.nextInt(10) + 1;
        this.welfare = 40 + randomGenerator.nextInt(10) + 1;
        this.tax = 5;
        this.neighbor = new HashSet<>();

    }



    public int taxes(){
        return (int) Math.round((population * tax * 2 * welfare * 0.5)/100 * (opposition>=90?0:1));
    }

    public void popilationTurn(){
        population = population + popTurn();
        if(population < 0) { population = 0; }
    }
    public int popTurn(){
        return (int) Math.round(((welfare<50)?0:population * welfare * 0.001));
    }

    public void welfareTurn(){
        welfare = welfare - welTurn();
        if( welfare > 100) { welfare = 100;}
        if( welfare < 0) { welfare = 0;}
    }
     public int welTurn(){
        return (int) Math.round(population * 0.005);
     }

    public void oppositionTurn(){

        long temp = Math.round(((population<200)?population * 0.007 * tax :population * 0.02 * tax));
        opposition = ((opposition + (int)(temp<1?1:temp)>100)?100:opposition + (int)(temp<1?1:temp));
    }
     public int oppTurn(){
         long temp = Math.round(((population<200)?population * 0.007 * tax :population * 0.02 * tax));
         return (int)(temp<1?1:temp);
     }

    public int electionTurn(){

        return (opposition > 50)?0:1;
    }


    public void setPopulation(int population) {
        if(population >= 0) {
            this.population = population;
        }
    }

    public void addNeighbor(Region region) {
        neighbor.add(region);
    }

    public void setOpposition(int opposition){
        if(opposition<=100 && opposition >= 0) {
            this.opposition = opposition;
        }
    }

    public void setTax(int tax) {
        if(tax >= 0) {
            this.tax = tax;
        }
        notifyObserver();
    }

    public void setWelfare(int welfare) {
        if (welfare>100) { this.welfare = 100;} else { this.welfare = welfare;}
    }




    public int getOpposition() {
        return opposition;
    }

    public int getPopulation() {
        return population;
    }

    public HashSet<Region> getNeighbor() {
        return neighbor;
    }

    public int getTax() {
        return tax;
    }

    public int getWelfare() {
        return welfare;
    }


    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + population;
        result = 31 * result + opposition;
        result = 31 * result + welfare;
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Region other = (Region) obj;
        if (opposition != other.getOpposition())
            return false;
        if (population != other.getPopulation())
            return false;
        if (welfare != other.getWelfare())
            return false;
        if (tax != other.getTax())
            return false;
        return true;
    }

    @Override
    public void addObserver(Observer obj) {
        observerSet.add(obj);
    }

    @Override
    public void notifyObserver() {
        for (Observer observer: observerSet) {
            observer.update();
        }

    }

}
