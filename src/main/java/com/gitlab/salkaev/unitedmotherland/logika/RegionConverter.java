package com.gitlab.salkaev.unitedmotherland.logika;
import com.google.gson.*;

import java.lang.reflect.Type;

public class RegionConverter implements JsonSerializer<Region>, JsonDeserializer<Region>{


    @Override
    public JsonElement serialize(Region region, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject object = new JsonObject();
        object.addProperty("population", region.getPopulation());
        object.addProperty("opposition", region.getOpposition());
        object.addProperty("welfare", region.getWelfare());
        object.addProperty("tax", region.getTax());


        return object;
    }

    @Override
    public Region deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        JsonObject jsonObject = jsonElement.getAsJsonObject();

        Region savedRegion = new Region();

        savedRegion.setPopulation(jsonObject.get("population").getAsInt());
        savedRegion.setOpposition(jsonObject.get("opposition").getAsInt());
        savedRegion.setWelfare(jsonObject.get("welfare").getAsInt());
        savedRegion.setTax(jsonObject.get("tax").getAsInt());

        return savedRegion;


    }
}
