package com.gitlab.salkaev.unitedmotherland.logika;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameConverter implements JsonSerializer<Game>, JsonDeserializer<Game>{


    @Override
    public JsonElement serialize(Game game, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject object = new JsonObject();
        object.addProperty("turn", game.getTurn());
        object.addProperty("money", game.getMoney());
        object.addProperty("elitePoint", game.getElitePoint());
        object.add("regions", jsonSerializationContext.serialize(game.getRegionMap()));
        return object;
    }

    @Override
    public Game deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {


        JsonObject jsonObject = jsonElement.getAsJsonObject();

        Game savedGame = new Game();
        savedGame.setTurn(jsonObject.get("turn").getAsInt());
        savedGame.setElitePoint(jsonObject.get("elitePoint").getAsInt());
        savedGame.setMoney(jsonObject.get("money").getAsInt());

        HashMap<String,Region> regionMap = new HashMap<>();

        JsonElement jel = jsonObject.get("regions");
        JsonObject job = jel.getAsJsonObject();

        for(Map.Entry<String, JsonElement> entry : job.entrySet()) {
            Region region = jsonDeserializationContext.deserialize(entry.getValue(), Region.class);
            regionMap.put(entry.getKey(), region);
        }


        savedGame.setRegionMap(regionMap);

        return savedGame;

    }
}
