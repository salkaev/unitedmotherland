package com.gitlab.salkaev.unitedmotherland.logika;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Market {

    private String name;
    private HashMap<String,Goods> GoodsMap = new HashMap<>();

    public Market(){

    }

    public HashMap<String, Goods> getGoodsMap() {
        return GoodsMap;
    }

    public String getName() {
        return name;
    }

    public void addToMarket(Goods goods){
        GoodsMap.put(goods.getName(),goods);
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getGoodsSet(){
        Set<String> goodsSet = new HashSet<>();
        for ( String key : GoodsMap.keySet() ) {
            goodsSet.add(key);
        }
        return goodsSet;
    }
}
