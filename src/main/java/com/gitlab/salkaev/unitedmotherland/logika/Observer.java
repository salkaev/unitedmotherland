package com.gitlab.salkaev.unitedmotherland.logika;

public interface Observer {

    void update();
}
