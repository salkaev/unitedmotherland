package com.gitlab.salkaev.unitedmotherland;

import com.gitlab.salkaev.unitedmotherland.logika.Game;
import com.gitlab.salkaev.unitedmotherland.logika.Region;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class GameTest {

    Game game;

    @Before
    public void setUp() {
        game = new Game();
    }

    @Test
    public void MainGameTest(){

        assertEquals(0,game.getTurn());
        assertEquals(1000, game.getMoney());
        assertEquals(0, game.getElitePoint());
        game.nextTurn();
        assertEquals(1,game.getTurn());
        assertTrue(game.getMoney() > 1000);
        assertEquals(0, game.getElitePoint());
        game.setTurn(10);
        game.setElitePoint(100);
        game.setMoney(1000);
        assertEquals(10,game.getTurn());
        assertEquals(100, game.getElitePoint());
        assertEquals(1000, game.getMoney());
    }

    @Test
    public void RegionTest(){

        Region region = game.getRegionMap().get("r1");

        assertEquals(5, region.getTax());
        region.setTax(10);
        assertEquals(10, region.getTax());
        region.setPopulation(1000);
        assertEquals(1000,region.getPopulation());
        region.setOpposition(4);
        assertEquals(4,region.getOpposition());
        region.setWelfare(1);
        assertEquals(1,region.getWelfare());


    }

}
